Package.describe({
  name: 'bigdata:logs',
  summary: ' /* Fill me in! */ ',
  version: '1.0.0',
  git: ' /* Fill me in! */ '
});

Package.onUse(function(api) {
  api.versionsFrom('1.0');
  api.addFiles('bigdata:logs.js');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('bigdata:logs');
  api.addFiles('bigdata:logs-tests.js');
});
